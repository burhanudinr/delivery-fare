# This is simple cli apps for calculate delivery fares

## Usage

![demo.gif](docs%2Fdemo.gif)

## Fare Calculation

```go
For the first 1 km: Rp 1000
Up to 10 km: Rp 1500
Up to 100 km: Rp 2000

example
total mileage 27 = (1 * 1000) + (9 * 1500) + (17 * 2000)
= 48.500

note: 9 * 1500 because upto 10 - 1km at first
```

## Solution to calculate fares just use simple logic if else
```go
func (d *Delivery) calculatePrice(_ context.Context, reached, mileage int) (int, error) {
	totalPrice := 0
	for kmAt := reached + 1; kmAt <= reached+mileage; kmAt++ {
		if kmAt == 1 {
			totalPrice += 1000
			continue
		}
		if kmAt <= 10 {
			totalPrice += 1500
			continue
		}
		if kmAt <= 100 {
			totalPrice += 2000
			continue
		}

		return 0, errors.New("fare calculation for above 100km is not set")
	}

	return totalPrice, nil
}
```

## Structure Directory
```bash
├── cmd
│   └── cli
│       ├── command
│       │   └── cli.go
│       └── main.go
├── go.mod
├── go.sum
├── initialize
│   └── base
│       └── init_logger.go
├── internal
│   ├── domain
│   │   └── cli
│   │       └── delivery
│   │           └── delivery_domain.go
│   ├── protocol
│   │   └── cli.go
│   ├── usecase
│   │   └── cli
│   │       └── delivery
│   │           ├── delivery.go
│   │           └── delivery_test.go
│   └── util
│       └── time_util.go
├── log
│   └── info.log
└── readme.md
```

## Unit testing coverage
![unittestcoverage.png](docs%2Funittestcoverage.png)

## Library
- Cobra for CLI
- sirupsen/logrus for Logger