package command

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"strconv"
	"strings"
	"test2/initialize/base"
	"test2/internal/protocol"
	"test2/internal/usecase/cli/delivery"
	"test2/internal/util"
	"time"
)

var (
	rootCmd = &cobra.Command{
		Use:        "cliapps",
		Short:      "cliapps root command",
		SuggestFor: []string{"cliapps"},
	}
)

func init() {
	rootCmd.AddCommand(
		NewRuleCommand(),
	)
}

func Start() {
	if err := rootCmd.Execute(); err != nil {
		if rootCmd.SilenceErrors {
			fmt.Fprintln(os.Stderr, "Error:", err)
			os.Exit(1)
		} else {
			os.Exit(1)
		}
	}
}

type Executor struct {
}

func NewRuleCommand() *cobra.Command {
	executor := Executor{}

	cmd := &cobra.Command{
		Use:        "farescalculator",
		Short:      "Delivery Fares Calculator",
		Long:       "this is simple cli apps for calculating delivery fee",
		Example:    "go run main.go delivery",
		SuggestFor: []string{"calc", "fares"},
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := executor.Execute(args); err != nil {
				return err
			}
			return nil
		},
	}
	return cmd
}

func (e *Executor) Execute(_ []string) error {
	var (
		file            = base.InitLogger()
		ctx             = context.Background()
		request         = protocol.CalcFareReq{}
		deliveryUsecase = delivery.NewDelivery()
		scanner         = bufio.NewScanner(os.Stdin)
	)
	defer file.Close()

	fmt.Println("enter the data")
	fmt.Println("example : {location} {time(hh:mm:ss.fff)} {mileage}")
	fmt.Println("example : PointA 00:05:00.100 5")
	fmt.Println("want to close ? write exit")
	fmt.Println("want to calculate ? write next")

Outer:
	for scanner.Scan() {
		switch scanner.Text() {
		case "exit":
			break Outer

		case "next":
			res, err := deliveryUsecase.Calculate(ctx, request)
			if err != nil {
				log.WithFields(log.Fields{
					"res":   res,
					"error": err.Error(),
				}).Error(err)
				fmt.Println(err.Error())
				break Outer
			}
			printRespCalcFares(ctx, res)
			break Outer

		default:
			req, err := composeRequestCalcFares(ctx, scanner.Text())
			if err != nil {
				log.WithFields(log.Fields{
					"input": scanner.Text(),
					"error": err.Error(),
				}).Error(err)
				fmt.Println(err.Error())
				break Outer
			}
			request.CalcFare = append(request.CalcFare, req)
		}
	}

	return nil
}

func composeRequestCalcFares(_ context.Context, input string) (protocol.CalcFare, error) {
	inputSplit := strings.Split(input, " ")
	if len(inputSplit) != 3 {
		return protocol.CalcFare{}, errors.New("input structure valid")
	}

	t, err := time.Parse("15:04:05", inputSplit[1])
	if err != nil {
		return protocol.CalcFare{}, errors.New("input date not valid")
	}

	mileage, err := strconv.Atoi(inputSplit[2])
	if err != nil {
		return protocol.CalcFare{}, errors.New("input mileage not valid")
	}

	return protocol.CalcFare{
		Location: inputSplit[0],
		Time:     t,
		Mileage:  mileage,
	}, nil
}

func printRespCalcFares(ctx context.Context, results protocol.CalcFareResp) {
	fmt.Println("=====OUTPUT=====")
	for _, result := range results.ResultString {
		fmt.Println(result)
	}

	fmt.Println(fmt.Sprintf("Total Price\t: Rp.%v", results.TotalPrice))
	fmt.Println(fmt.Sprintf("Total Mileage\t: %v", results.TotalMileage))
	fmt.Println(fmt.Sprintf("Total Duration\t: %v", util.PrintTime(results.TotalDuration)))
}
