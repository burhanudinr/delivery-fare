package base

import (
	log "github.com/sirupsen/logrus"
	"os"
)

func InitLogger() *os.File {
	file, err := os.OpenFile("../../log/info.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	log.SetOutput(file)
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.WarnLevel)

	return file
}
