package util

import (
	"fmt"
	"time"
)

func PrintTime(t time.Time) string {
	return fmt.Sprintf("%02d:%02d:%02d.%d", t.Hour(), t.Minute(), t.Second(), t.Nanosecond()/10000000)
}
