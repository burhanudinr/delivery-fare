package delivery

import (
	"context"
	"reflect"
	"test2/internal/protocol"
	"testing"
	"time"
)

func TestDelivery_Calculate(t *testing.T) {
	timeReq, _ := time.Parse("15:04:05", "00:05:00.100")
	timeResp := time.Time{}.Add(time.Minute * 15).Add(time.Millisecond * 300)

	type args struct {
		ctx context.Context
		req protocol.CalcFareReq
	}
	tests := []struct {
		name    string
		args    args
		want    protocol.CalcFareResp
		wantErr bool
	}{
		{
			name: "test_success",
			args: args{
				ctx: context.Background(),
				req: protocol.CalcFareReq{
					CalcFare: []protocol.CalcFare{
						{
							Location: "PointA",
							Time:     timeReq,
							Mileage:  5,
						}, {
							Location: "PointB",
							Time:     timeReq,
							Mileage:  8,
						},
						{
							Location: "PointC",
							Time:     timeReq,
							Mileage:  10,
						},
					},
				},
			},
			want: protocol.CalcFareResp{
				ResultString: []string{
					"Location: PointA,\t Time: 00:05:00.10,\t Mileage: 5,\t Total Price: Rp. 7000",
					"Location: PointB,\t Time: 00:05:00.10,\t Mileage: 8,\t Total Price: Rp. 13500",
					"Location: PointC,\t Time: 00:05:00.10,\t Mileage: 10,\t Total Price: Rp. 20000",
				},
				TotalMileage:  23,
				TotalPrice:    40500,
				TotalDuration: timeResp,
			},
			wantErr: false,
		}, {
			name: "test_fail input time",
			args: args{
				ctx: context.Background(),
				req: protocol.CalcFareReq{
					CalcFare: []protocol.CalcFare{
						{
							Location: "PointA",
							Time:     timeReq.Add(time.Minute * 10),
							Mileage:  5,
						},
					},
				},
			},
			want:    protocol.CalcFareResp{},
			wantErr: true,
		}, {
			name: "test_fail input milage is zero",
			args: args{
				ctx: context.Background(),
				req: protocol.CalcFareReq{
					CalcFare: []protocol.CalcFare{
						{
							Location: "PointA",
							Time:     timeReq,
							Mileage:  0,
						},
					},
				},
			},
			want:    protocol.CalcFareResp{},
			wantErr: true,
		}, {
			name: "test_fail total distance above 100km",
			args: args{
				ctx: context.Background(),
				req: protocol.CalcFareReq{
					CalcFare: []protocol.CalcFare{
						{
							Location: "PointA",
							Time:     timeReq,
							Mileage:  101,
						},
					},
				},
			},
			want:    protocol.CalcFareResp{},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := NewDelivery()
			got, err := d.Calculate(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("Calculate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Calculate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
