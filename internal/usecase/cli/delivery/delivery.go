package delivery

import (
	"context"
	"errors"
	"fmt"
	"test2/internal/domain/cli/delivery"
	"test2/internal/protocol"
	"test2/internal/util"
	"time"
)

type Delivery struct {
}

type DeliveryInterface interface {
	Calculate(ctx context.Context, req protocol.CalcFareReq) (protocol.CalcFareResp, error)
}

func NewDelivery() *Delivery {
	return &Delivery{}
}

func (d *Delivery) Calculate(ctx context.Context, req protocol.CalcFareReq) (protocol.CalcFareResp, error) {
	var (
		reached = 0
		results = protocol.CalcFareResp{}
	)

	if err := req.Validate(); err != nil {
		return results, err
	}

	for _, point := range req.CalcFare {
		price, err := d.calculatePrice(ctx, reached, point.Mileage)
		if err != nil {
			return results, err
		}
		reached += point.Mileage

		results.TotalPrice += price
		results.TotalMileage += point.Mileage
		results.TotalDuration = results.TotalDuration.
			Add(time.Hour * time.Duration(point.Time.Hour())).
			Add(time.Minute * time.Duration(point.Time.Minute())).
			Add(time.Second * time.Duration(point.Time.Second())).
			Add(time.Nanosecond * time.Duration(point.Time.Nanosecond()))

		results.ResultString = append(results.ResultString,
			fmt.Sprintf(delivery.FormatDeliveryFares, point.Location, util.PrintTime(point.Time), point.Mileage, price))
	}

	return results, nil
}

func (d *Delivery) calculatePrice(_ context.Context, reached, mileage int) (int, error) {
	totalPrice := 0
	for kmAt := reached + 1; kmAt <= reached+mileage; kmAt++ {
		if kmAt == 1 {
			totalPrice += 1000
			continue
		}
		if kmAt <= 10 {
			totalPrice += 1500
			continue
		}
		if kmAt <= 100 {
			totalPrice += 2000
			continue
		}

		return 0, errors.New("fare calculation for above 100km is not set")
	}

	return totalPrice, nil
}
