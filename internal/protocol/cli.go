package protocol

import (
	"errors"
	"time"
)

type CalcFareReq struct {
	CalcFare []CalcFare
}

func (c CalcFareReq) Validate() error {
	sumMileage := 0
	for _, val := range c.CalcFare {
		if val.Time.Hour() > 0 || val.Time.Minute() < 2 || val.Time.Minute() >= 10 {
			return errors.New("input time has a range of less than 2 minutes or more than 10 minutes")
		}
		sumMileage += val.Mileage
	}

	if sumMileage == 0 {
		return errors.New("total mileage is zero")
	}

	return nil
}

type CalcFare struct {
	Location string
	Time     time.Time
	Mileage  int
}

type CalcFareResp struct {
	ResultString  []string
	TotalMileage  int
	TotalPrice    int
	TotalDuration time.Time
}
